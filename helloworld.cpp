#include <iostream>
#include <vector>
#include <unistd.h>
#include <stdio.h>
using namespace std;

int main()
{
	string line;

	for (int j=0; j<13; j++)
	{
		switch (j)
		{
			case(0): line = " __    __   ______   __       __       _______";
			         break;
			case(1): line = "|  |  |  | |   ___| |  |     |  |     |   _   |";
			         break;
			case(2): line = "|  |__|  | |  |_    |  |     |  |     |  | |  |";
			         break;
			case(3): line = "|   __   | |   _|   |  |     |  |     |  | |  |";
			         break;
			case(4): line = "|  |  |  | |  |___  |  |___  |  |___  |  |_|  |  ___";
			         break;
			case(5): line = "|__|  |__| |______| |______| |______| |_______| |_  |";
			         break;	
			case(6): line = "                                                 /_/";
			         break;	
			case(7): line = " __    __   _______   _______   __       _____     __";
			         break;	
			case(8): line = "|  |  |  | |   _   | |  ___  | |  |     |     \\   |  |";
			         break;	
			case(9): line = "|  |/\\|  | |  | |  | | |___| | |  |     |  |\\  \\  |  |";
			         break;	
			case(10): line = "|        | |  | |  | |      _| |  |     |  | |  | |__|";
			          break;	
			case(11): line = "|   /\\   | |  |_|  | |  |\\  \\  |  |___  |  |/  /   __";
			          break;	
			case(12): line = "|__/  \\__| |_______| |__| \\__\\ |______| |_____/   |__|";
			          break;						 
		}
		
		vector<char> hw(line.begin(), line.end());
		for (int i=0; i<hw.size(); i++)
        {
            cout << hw[i];
		    fflush(stdout);
            usleep(10000);
        }
		cout << endl;
	}
	cout << endl;
	return 0;
}